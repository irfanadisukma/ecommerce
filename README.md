# E-commerce App

- Infinite Pagination
- Cart Function (Local Storage)
- Auto Generated Invoice Number (Date, Random String)
- REST API Data source using IMDB

## Getting Started

For help getting started with Flutter, view online
[documentation](https://flutter.dev/).

#### 1. [Setup Flutter](https://flutter.dev/setup/)

#### 2. Clone the repo

```sh
$ git clone https://gitlab.com/irfanadisukma/dekornatadev.git
```

#### 4. Before run project, execute commands:

```sh
$ flutter clean
$ flutter pub get
$ flutter run` (if needed)
