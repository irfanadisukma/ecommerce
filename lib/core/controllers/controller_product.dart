// ignore_for_file: avoid_print

part of 'controllers.dart';

final controllerProduct =
    ChangeNotifierProvider<ControllerProduct>((ref) => ControllerProduct());

class ControllerProduct extends ChangeNotifier {
  ProductListModel? productListData;
  ProductDetailModel? productDetailData;
  List<String> productImagesData = [];
  int currentPage = 1;
  bool isLoading = false;

  Future getProductList({int? prevPage, int? nextPage}) async {
    try {
      var endpoint = ApiRoute().productListByCategory + "api_key=$apiToken&with_genres=12&page=$nextPage";
      http.Response response = await http.get(
        Uri.parse(endpoint),
      );


      if(response.statusCode == 200){
         // Checking Page, if current page is not same as the next page, then add new values to list
        if(nextPage != 1 && prevPage != nextPage){
          ProductListModel newData = productListModelFromJson(response.body);
          productListData!.results!.addAll(newData.results!);
        } else {
          productListData = productListModelFromJson(response.body);
        }
      } else {
        productListData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }


  Future getProductByID({required int productId}) async {
    try {
      var endpoint = "${ApiRoute().productById}$productId?api_key=$apiToken";
      http.Response response = await http.get(
        Uri.parse(endpoint),
      );
      

      if(response.statusCode == 200){
        productDetailData = productDetailModelFromJson(response.body);
        productImagesData.add(productDetailData!.posterPath!);
        productImagesData.add(productDetailData!.backdropPath!);

      } else {
        productDetailData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }



}
