// ignore_for_file: avoid_print

part of 'controllers.dart';

final controllerCart =
    ChangeNotifierProvider<ControllerCart>((ref) => ControllerCart());



class ControllerCart extends ChangeNotifier {
  List<ListCartModel> cartData = [];
  bool allCheckStatus = false;
  int totalPrice = 0;


  Future getCartData() async {
    SharedPrefManager().getStringValueSF(KEY_CART_DATA).then((value) {
      if(value == null){
        cartData == [];
      } else {
        cartData = listCartModelFromJson(value);
      }
    });
    totalPrice = 0;
    retrieveTotalPrice();
    notifyListeners();
  }

  Future updateCart(String newData) async {
    SharedPrefManager().addStringToSF(KEY_CART_DATA,newData);
    getCartData();
  }

  Future addToCart(ProductDetailModel dataProduct) async {
   
    List<ListCartModel> carts = [];
    if(cartData.isNotEmpty){
        carts = cartData;
    }
    ListCartModel cart = ListCartModel(
      id: dataProduct.id!, 
      name: dataProduct.title!, 
      image: dataProduct.posterPath!, 
      price: dataProduct.id!, 
      stock: dataProduct.voteCount!,
      qty: 1,
      checked: false
    );  

    carts.add(cart);    
    var data = listCartModelToJson(carts);
    SharedPrefManager().addStringToSF(KEY_CART_DATA, data);
    getCartData();
  }

  Future addToCartFromItem(Result dataProduct) async {
    
    List<ListCartModel> carts = [];
    if(cartData.isNotEmpty){
        carts = cartData;
    }
    ListCartModel cart = ListCartModel(
      id: dataProduct.id!, 
      name: dataProduct.title!, 
      image: dataProduct.posterPath!, 
      price: dataProduct.id!, 
      stock: dataProduct.voteCount!,
      qty: 1,
      checked: false
    );  

    carts.add(cart);
                        
    var data = listCartModelToJson(carts);
    SharedPrefManager().addStringToSF(KEY_CART_DATA, data);
    getCartData();
    
  }

  Future checkCart(ListCartModel singleData, bool checkStatus) async {
    for(int i =0; i<cartData.length; i++){
      if(cartData[i].id == singleData.id){
        cartData[i].checked = checkStatus;
      }
    }
    allCheckStatus = false;
    var newData = listCartModelToJson(cartData);
    updateCart(newData);
  }

  Future checkAllCart(bool checkStatus) async {
    for (var element in cartData) { 
      element.checked = checkStatus;
    }
    var newData = listCartModelToJson(cartData);
    updateCart(newData);
  }

   Future updateItemQty(int itemId, int qty) async {
    for (var element in cartData) { 
      if(element.id == itemId) {
        element.qty = qty;
      }
    }
    var newData = listCartModelToJson(cartData);
    updateCart(newData);
  }

  Future deleteSelectedCart() async {
    cartData.removeWhere((element) => element.checked == true);
    allCheckStatus = false;
    var newData = listCartModelToJson(cartData);
    updateCart(newData);
  }

  Future deleteSingleCart(int itemId) async {
    cartData.removeWhere((element) => element.id == itemId);
    var newData = listCartModelToJson(cartData);
    updateCart(newData);
  }

  Future clearCart() async {
    cartData = [];
    totalPrice = 0;
    allCheckStatus = false;
    retrieveTotalPrice();
    SharedPrefManager().clearSF();
    notifyListeners();
  }

  Future retrieveTotalPrice() async {
   for (var element in cartData) { 
     if(element.checked){
       totalPrice = totalPrice + element.qty * element.price;
     }
    }
    notifyListeners();
  }

  

  

}
