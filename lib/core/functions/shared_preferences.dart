part of 'functions.dart';

class SharedPrefManager {

  // ADD VALUE

  Future<String> addStringToSF(String key, String? value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value!);
    return "success";
  }

  Future<int> addIntToSF(String key, int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
    return 1;
  }

  Future<double> addDoubleToSF(String key, double value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble(key, value);
    return 1;
  }

  Future<bool> addBoolToSF(String key, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
    return true;
  }

  // READ VALUE
  Future<String?> getStringValueSF(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<bool?> getBoolValueSF(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  Future<int?> getIntValueSF(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  Future<double?> getDoubleValueSF(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getDouble(key);
  }

  // CLEAR PREF
  clearSF() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }
}
