
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ecommerce/ui/widgets/widgets.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'shared_preferences.dart';
part 'shared_value.dart';

class Functions {
  static String rupiah(value, {String separator = '.', String trailing = ''}) {
    return "IDR " + value.toString().replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}$separator') + trailing;
  }


  static String generateinvoice(){
    DateTime now = DateTime.now();
    var _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    final Random _rnd = Random();
    String formattedDate = DateFormat('yyyyMMdd').format(now);
    var randString = String.fromCharCodes(Iterable.generate(6, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
    var invoice = "INVOICE/$formattedDate/ECOMMERCE/${randString.toUpperCase()}";
    return invoice;
  }

  static void showLoading(BuildContext context, bool dismissable) {
    showDialog(
      context: context,
      barrierDismissible: dismissable,
      builder: (BuildContext context) => const DialogLoading(),
    );
  }

}