// ignore_for_file: prefer_if_null_operators, unnecessary_null_comparison

part of 'models.dart';

List<ListCartModel> listCartModelFromJson(String str) => List<ListCartModel>.from(json.decode(str).map((x) => ListCartModel.fromJson(x)));

String listCartModelToJson(List<ListCartModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ListCartModel {
    ListCartModel({
        required this.id,
        required this.name,
        required this.image,
        required this.price,
        required this.stock,
        required this.qty,
        required this.checked
    });

    int id;
    String name;
    String image;
    int price;
    int stock;
    int qty;
    bool checked;

    factory ListCartModel.fromJson(Map<String, dynamic> json) => ListCartModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        image: json["image"] == null ? null : json["image"],
        price: json["price"] == null ? null : json["price"],
        stock: json["stock"] == null ? null : json["stock"],
        qty: json["qty"] == null ? null : json["qty"],
        checked: json["checked"] == null ? null : json["checked"]
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "image": image == null ? null : image,
        "price": price == null ? null : price,
        "stock": stock == null ? null : stock,
        "qty": qty == null ? null : qty,
        "checked": checked == null ? null : checked
    };
}
