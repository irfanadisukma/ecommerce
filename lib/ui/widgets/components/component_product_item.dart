part of '../widgets.dart';

class ComponentProductItem extends ConsumerWidget {
  const ComponentProductItem({Key? key, this.productData, this.onClick, this.onClickAddCart}) : super(key: key);

  final Result? productData;
  final Function? onClick;
  final Function? onClickAddCart;

  @override
  Widget build(BuildContext context, ref) {
    final cartController = ref.watch(controllerCart);
    return InkWell(
      onTap: () => onClick!(),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 1,
                offset: const Offset(0, 0),
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                child: Image.network(
                  baseUrlImage + productData!.posterPath!,
                  width: MediaQuery.of(context).size.width,
                  height: 150,
                  fit: BoxFit.cover
                ),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(productData!.title!,
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: GoogleFonts.dosis(
                    color: Colors.black87,
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  )
                ),
              ),
              const SizedBox(height: 2),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(
                  Functions.rupiah(productData!.id), 
                  style: GoogleFonts.poppins(
                    color: Colors.blue[900],
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                  )
                ),
              ),
              const SizedBox(height: 2),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(
                  "Stok ${productData!.voteCount}", 
                  style: GoogleFonts.dosis(
                    color: Colors.red[400],
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  )
                ),
              ),
              const SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                  elevation: 0.8,
                  height: 30,
                  onPressed: ()  {
                    cartController.addToCartFromItem(productData!);
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text("Item added to cart"))
                    );
                  },
                  color: Colors.blue[900],
                  child: Center(
                    child: Text(
                      "+ KERANJANG",
                      style: GoogleFonts.dosis(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      )
                    ),
                  ),
                  textColor: Colors.white,
                ),
              ),
              const SizedBox(height: 5),
            ]
          )
        ),
      ),
    );
  }
}
