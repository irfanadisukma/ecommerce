part of '../widgets.dart';

class ComponentConfirmationItem extends ConsumerWidget {
  const ComponentConfirmationItem({Key? key, this.cartData,}) : super(key: key);

  final ListCartModel? cartData;

  @override
  Widget build(BuildContext context, ref) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          const Divider(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.network(
                baseUrlImage + cartData!.image,
                width: 80,
                height: 80,
                fit: BoxFit.cover
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        cartData!.name,
                        maxLines: 2,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.dosis(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          )
                        ),
                    ),
                    const SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        Functions.rupiah(cartData!.id), 
                        style: GoogleFonts.dosis(
                          color: Colors.blue[900],
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                        )
                      ),
                    ),
                    const SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        "Jumlah: ${cartData!.qty}", 
                        style: GoogleFonts.dosis(
                          color: Colors.grey,
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        )
                      ),
                    ),
                  ]
                ),
              )
            ]
          )
        ]
      ),
    );
  }
}



