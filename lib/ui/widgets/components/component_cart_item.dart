part of '../widgets.dart';

class ComponentPartItem extends ConsumerWidget {
  const ComponentPartItem({Key? key, this.cartData, this.onClick}) : super(key: key);

  final ListCartModel? cartData;
  final Function? onClick;

  @override
  Widget build(BuildContext context, ref) {
    final cartController = ref.watch(controllerCart);
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration( color: Colors.white),
      child: CheckboxListTile(
        title: Column(
          children: [
            const Divider(height: 20),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.network(
                  baseUrlImage + cartData!.image,
                  width: 80,
                  height: 80,
                  fit: BoxFit.cover
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          cartData!.name,
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.dosis(
                            color: Colors.black87,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          )
                        ),
                      ),
                      const SizedBox(height: 5),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          Functions.rupiah(cartData!.id), 
                          style: GoogleFonts.dosis(
                            color: Colors.blue[900],
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                          )),
                      ),
                      const SizedBox(height: 5),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          "Stok ${cartData!.stock}", 
                          style: GoogleFonts.dosis(
                            color: Colors.grey,
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          )
                        ),
                      ),
                    ]
                  )
                )
              ]
            ),
            const SizedBox(height: 10),

            Row(
              children: [
                const Spacer(),
                InkWell(
                  onTap: () => cartController.deleteSingleCart(cartData!.id), 
                  child: const Icon(Icons.delete_outlined, size: 18, color: Colors.black54)
                ),
                const SizedBox(width: 10),
                InkWell(
                  onTap: (){
                    if(cartData!.qty > 1){
                      cartController.updateItemQty(cartData!.id, cartData!.qty-1);
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey[200]
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Icon(Icons.remove, size: 15),
                    )
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    "${cartData!.qty}",
                    style: GoogleFonts.dosis(
                      color: Colors.black87,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    )
                  )
                ),
                InkWell(
                  onTap: (){
                    cartController.updateItemQty(cartData!.id, cartData!.qty+1);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey[200]
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Icon(Icons.add, size: 15),
                    )
                  )
                )
              ]),
            const SizedBox(height: 10),
          ]
        ),
        value: cartData!.checked,
        onChanged: (newValue) {
          cartController.checkCart(cartData!, newValue!);
        },
        controlAffinity: ListTileControlAffinity.leading,
      )
    );
  }
}



