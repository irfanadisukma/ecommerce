part of '../widgets.dart';

class ComponentItemOrderSummary extends ConsumerWidget {
  const ComponentItemOrderSummary({Key? key, this.cartData,}) : super(key: key);

  final ListCartModel? cartData;

  @override
  Widget build(BuildContext context, ref) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration( color: Colors.white),
      child: Column(
        children: [
          const SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        cartData!.name,
                        maxLines: 2,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        style: GoogleFonts.dosis(
                          color: Colors.black87,
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                        )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        Functions.rupiah(cartData!.id), 
                        style: GoogleFonts.dosis(
                          color: Colors.blue[900],
                          fontSize: 10,
                          fontWeight: FontWeight.w700,
                        )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        "Jumlah: ${cartData!.qty}", 
                        style: GoogleFonts.dosis(
                          color: Colors.grey,
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                        )
                      ),
                    ),
                  ]
                ),
              )
            ]
          ), 
        ]
      ),
    );
  }
}



