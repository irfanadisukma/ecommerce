// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenProductList extends ConsumerWidget {
  const ScreenProductList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final controller = ref.watch(controllerProduct);
    final cartController = ref.watch(controllerCart);

    if(controller.productListData == null){
      controller.getProductList(prevPage: 1, nextPage: controller.currentPage);
    }
   
    return WillPopScope(
      onWillPop: () async {
        controller.productListData = null;
        controller.currentPage = 1; 
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          centerTitle: false,
          backgroundColor: Colors.white,
          title: Text(
            "e-commerce",
              style: GoogleFonts.dosis(
              color: Colors.blue[900],
              fontSize: 18,
              fontWeight: FontWeight.w700,
              )
            ),
            actions: <Widget>[
              Badge(
                badgeColor: Colors.orange,
                badgeContent: Text(
                  "${cartController.cartData.length}",
                  style: GoogleFonts.dosis(
                    color: Colors.white,
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                position: BadgePosition.topEnd(top: 8, end: 8),
                child: IconButton(
                  icon: Icon(
                    Icons.shopping_basket_rounded,
                    color: Colors.blue[900],
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenCartList()));
                  },
                ),
              )
            ],
          ),
          
          body: Stack(
            children: [
              (controller.productListData != null) ? (controller.productListData!.results!.isEmpty) ? 
              Container(
                alignment: Alignment.center, 
                margin: const EdgeInsets.only(top: 50.0), 
                child: Text("No Products Found")
              ) : Container(
                margin: const EdgeInsets.only(top: 0.0),
                child: NotificationListener<ScrollUpdateNotification>(
                  onNotification: (notification) {
                    if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                      // start loading data
                      if (controller.productListData!.results!.length != controller.productListData!.totalResults) {
                        var prevPage = controller.currentPage;
                        controller.isLoading = true;
                        controller.currentPage = controller.currentPage + 1;
                        controller.getProductList(prevPage: prevPage, nextPage: controller.currentPage);
                      } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('You are at the end of the list')));
                        }
                      }
                    return true;
                  },
                  child: StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    crossAxisCount: 4,
                    itemCount: controller.productListData!.results!.length,
                    physics: const ClampingScrollPhysics(),
                    staggeredTileBuilder: (int index) => const StaggeredTile.fit(2),
                    itemBuilder: (context, index) {
                      return ComponentProductItem(
                        productData: controller.productListData!.results![index],
                        onClick: () {
                          controller.productDetailData = null;
                          controller.productImagesData = [];
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenProductDetail(productData: controller.productListData!.results![index])));
                        } 
                        );
                      },
                    )),
                  )
                : Center(child: const CircularProgressIndicator()
              ),
            ]
          )
        ),
    );
  }
}
