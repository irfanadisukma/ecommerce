// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenProductDetail extends ConsumerStatefulWidget {
  const ScreenProductDetail({Key? key, required this.productData})
      : super(key: key);
  final Result productData;

  @override
  _ScreenProductDetailState createState() => _ScreenProductDetailState();
}

class _ScreenProductDetailState extends ConsumerState<ScreenProductDetail> {
  TextEditingController controllerQuantity = TextEditingController();
  final imageViewController = PageController();

  @override
  Widget build(BuildContext context) {
    final productController = ref.watch(controllerProduct);
    final cartController = ref.watch(controllerCart);
    if(productController.productDetailData == null){
       productController.getProductByID(productId: widget.productData.id!);
       
    }
    return WillPopScope(
      onWillPop: () async {
        productController.productDetailData = null;
        productController.productImagesData = [];
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              productController.productDetailData = null;
              productController.productImagesData = [];
              Navigator.of(context).pop();
            }
          ),
          actions: <Widget>[
            Badge(
              badgeColor: Colors.orange,
              badgeContent: Text(
                "${cartController.cartData.length}",
                style: GoogleFonts.dosis(
                  color: Colors.white,
                  fontSize: 10,
                  fontWeight: FontWeight.w600,
                  ),
              ),
              position: BadgePosition.topEnd(top: 8, end: 8),
              child: IconButton(
                icon: Icon(
                  Icons.shopping_basket_rounded,
                  color: Colors.white,
                ),
                onPressed: () {
                   Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenCartList()));
                },
              ),
            )
              
          ],
          centerTitle: false,
          backgroundColor: Colors.blue[900],
          title: Text(
            widget.productData.title!,
            style: GoogleFonts.dosis(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w700,
            )
          ),
        ),
        body: productController.productDetailData != null ? Stack(
          children: [
            SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.width * 0.9,
                      child: Stack(
                        children: [
                          PageView.builder(
                            itemCount: productController.productImagesData.length,
                            controller: imageViewController,
                            itemBuilder: (BuildContext context,int index) {
                              return Image.network(
                                baseUrlImage + productController.productDetailData!.posterPath!,
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.width * 0.9,
                                fit: BoxFit.cover
                              );
                            },
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0),
                              child: SmoothPageIndicator(
                                controller: imageViewController,
                                count: productController.productImagesData.length,
                                axisDirection: Axis.horizontal,
                                onDotClicked: (i) {
                                  imageViewController.animateToPage(i, duration: Duration(milliseconds: 500), curve: Curves.ease);
                                },
                                effect: ExpandingDotsEffect(
                                  expansionFactor: 2,
                                  spacing: 5,
                                  radius: 8,
                                  dotWidth: 8,
                                  dotHeight: 8,
                                  dotColor: Colors.grey,
                                  activeDotColor: Colors.blue,
                                  paintStyle: PaintingStyle.fill,
                                ),
                              ),
                            ),
                          ),
                        ]
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                productController.productDetailData!.title!,
                                style: GoogleFonts.dosis(
                                  color: Colors.blue[900],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                )
                              ),
                              const SizedBox(height: 10),
                              Text(
                                Functions.rupiah(productController.productDetailData!.id), 
                                style: GoogleFonts.dosis(
                                  color: Colors.blue[900],
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                )
                              ),
                              const SizedBox(height: 5),
                              Text(
                                "Stok: ${productController.productDetailData!.runtime}", 
                                style: GoogleFonts.dosis(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )
                              ),
            
                              Divider(height: 20),
            
                              Text(
                                "Deskripsi", 
                                style: GoogleFonts.dosis(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                )
                              ),
                              const SizedBox(height: 5),
                              Text(
                                "Stok ${productController.productDetailData!.overview}", 
                                style: GoogleFonts.dosis(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )
                              ),
                              Divider(height: 30),
            
                              Text(
                                "PRODUK REKOMENDASI", 
                                style: GoogleFonts.dosis(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                )
                              ),
                              const SizedBox(height: 5),
                            ]
                          ),
                        ),
                        StaggeredGridView.countBuilder(
                          shrinkWrap: true,
                          crossAxisCount: 4,
                          itemCount: 6,
                          physics: const ClampingScrollPhysics(),
                          staggeredTileBuilder: (int index) => const StaggeredTile.fit(2),
                          itemBuilder: (context, index) {
                            return ComponentProductItem(
                              productData: productController.productListData!.results![index],
                              onClick: () {
                                productController.productDetailData = null;
                                productController.productImagesData = [];
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenProductDetail(productData: productController.productListData!.results![index])));
                              } 
                              );
                            },
                          ),
                        const SizedBox(height: 60),
                    ]
                  )
                  ]
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 25),
                child: InkWell(
                  onTap: () async {
                    cartController.addToCart(productController.productDetailData!);
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Item added to cart")));
                  },
                  child: Container(
                    height: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blue[900]
                    ),
                    child: Text("ADD TO CART",
                      style: GoogleFonts.dosis(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      )
                    )
                  ),
                )
              ),
             ),
          ]
        ) : Center(child: const CircularProgressIndicator())
      ),
    );
  }
}





