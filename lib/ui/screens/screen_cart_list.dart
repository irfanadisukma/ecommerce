part of 'screens.dart';

class ScreenCartList extends ConsumerWidget {
  const ScreenCartList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final cartController = ref.watch(controllerCart);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          }
        ),
        title: Text("KERANJANG",
          style: GoogleFonts.dosis(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w700,
          )
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 200,
                        child: CheckboxListTile(
                          contentPadding: const EdgeInsets.all(0),
                          title: Text(
                            "Pilih Semua",
                            style: GoogleFonts.dosis(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                )
                          ),
                          value: cartController.allCheckStatus,
                          onChanged: (newValue) {
                            cartController.allCheckStatus = newValue!;
                            cartController.checkAllCart(newValue);
                          },
                          controlAffinity: ListTileControlAffinity.leading,

                        ),
                      ),
                      InkWell(
                        onTap: () => cartController.deleteSelectedCart(),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.blue[900]
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
                            child: Text(
                              "Hapus Pilihan",
                              style: GoogleFonts.dosis(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                          )
                        ),
                      )
            
                    ]
                  ),
                ),

                cartController.cartData.isNotEmpty ? ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  physics: const NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: cartController.cartData.length,
                  itemBuilder: (BuildContext context, int index) =>
                    ComponentPartItem(
                      cartData: cartController.cartData[index],
                      onClick: (){}
                    ),
                ) : SizedBox(
                  height: 200,
                  child: Center(
                    child: Text(
                      "Tidak ada produk di keranjang",
                      style: GoogleFonts.dosis(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      )
                    )
                  )
                ),
                const SizedBox(height: 100),
              ]
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
                child: Wrap(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                        children: [
                          Text(
                            "Total Harga",
                            style: GoogleFonts.dosis(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            )
                          ),
                          Text(
                            Functions.rupiah(cartController.totalPrice),
                            style: GoogleFonts.dosis(
                              color: Colors.blue[900],
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            )
                          )
                        ]
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if(cartController.totalPrice != 0){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ScreenConfirmationOrder()));
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.blue[900]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                          child: Text(
                            "BELI PRODUK",
                            style: GoogleFonts.dosis(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            )
                          )
                        )
                      ),
                    )
                  ]
                ),
              )
            ),
          )
        ]
      )
    );
  }
}
