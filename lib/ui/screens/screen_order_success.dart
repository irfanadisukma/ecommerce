// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenOrderSuccess extends ConsumerStatefulWidget {
  const ScreenOrderSuccess({Key? key}) : super(key: key);
  

  @override
  _ScreenOrderSuccessState createState() => _ScreenOrderSuccessState();
}

class _ScreenOrderSuccessState extends ConsumerState<ScreenOrderSuccess> {
  TextEditingController controllerQuantity = TextEditingController();
  final imageViewController = PageController();

  @override
  Widget build(BuildContext context) {
    final cartController = ref.watch(controllerCart);
    return WillPopScope(
      onWillPop: () async {
        cartController.clearCart();
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => ScreenProductList()), (Route<dynamic> route) => false);
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.close, color: Colors.white),
            onPressed: () {
              cartController.clearCart();
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => ScreenProductList()), (Route<dynamic> route) => false);
              }
          ),
          centerTitle: false,
          backgroundColor: Colors.blue[900],
          title: Text(
            "ORDER FINISHED",
            style: GoogleFonts.dosis(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w700,
            )
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 15),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        child: Column(
                          children: [
                            Text(
                              "ORDER CREATED",
                              style: GoogleFonts.dosis(
                                color: Colors.blue[900],
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                            const SizedBox(height: 10),
                            Text(
                              Functions.generateinvoice(),
                              style: GoogleFonts.dosis(
                                color: Colors.blue[900],
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              )
                            ),
                            SizedBox(height: 10),
                            Text(
                              "Thanks for your Order!",
                              style: GoogleFonts.dosis(
                              color: Colors.black87,
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              )
                            )
                          ]
                        ),
                      )
                    ),
                    Divider(height: 20),
                    Text(
                      "ORDER SUMMARY",
                      style: GoogleFonts.dosis(
                        color: Colors.black87,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      )
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: cartController.cartData.length,
                      itemBuilder: (BuildContext context, int index) =>
                        (cartController.cartData[index].checked) ? ComponentItemOrderSummary(
                          cartData: cartController.cartData[index],
                        ) : SizedBox(),
                      ),
                    Divider(),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15.0, top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                        children: [
                          Text(
                            "Total Dibayarkan",
                            style: GoogleFonts.dosis(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            )
                          ),
                          Text(
                            Functions.rupiah(cartController.totalPrice + 14000),
                            style: GoogleFonts.dosis(
                              color: Colors.blue[900],
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            )
                          )
                        ]
                      ),
                    ),
                    SizedBox(height: 10),
                    InkWell(
                      onTap: () {
                        cartController.clearCart();
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => ScreenProductList()), (Route<dynamic> route) => false);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.blue[900]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                          child: Text(
                            "Tutup",
                            style: GoogleFonts.dosis(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            )
                          ),
                        )
                      ),
                    ),
                    const SizedBox(height: 150),
                  ]
                )
              )
            )
          ]
        )
      )
    );
  }
}





