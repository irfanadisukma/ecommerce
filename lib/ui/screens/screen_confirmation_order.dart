// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenConfirmationOrder extends ConsumerStatefulWidget {
  const ScreenConfirmationOrder({Key? key}) : super(key: key);
  

  @override
  _ScreenConfirmationOrderState createState() => _ScreenConfirmationOrderState();
}

class _ScreenConfirmationOrderState extends ConsumerState<ScreenConfirmationOrder> {
  TextEditingController controllerQuantity = TextEditingController();
  final imageViewController = PageController();

  @override
  Widget build(BuildContext context) {
    final cartController = ref.watch(controllerCart);
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            }
          ),
          centerTitle: false,
          backgroundColor: Colors.blue[900],
          title: Text(
            "CONFIRMATION ORDER",
            style: GoogleFonts.dosis(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w700,
            )
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 15),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.yellow.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Irfan Adisukma",
                                  style: GoogleFonts.dosis(
                                    color: Colors.blue[900],
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                  )
                                ),
                                SizedBox(width: 10),
                                Text(
                                  "[082211144633]",
                                  style: GoogleFonts.dosis(
                                    color: Colors.blue[900],
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  )
                                ),  
                              ]
                            ),
                            const SizedBox(height: 5),
                            Text(
                              "Jl Jaya Wijaya 1 No 13, Kecamatan Kebayoran Baru, Kota Jakarta Selatan, DKI Jakarta, 15642",
                              style: GoogleFonts.dosis(
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              )
                            ),
                          ]
                        ),
                      )
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: cartController.cartData.length,
                      itemBuilder: (BuildContext context, int index) =>
                        (cartController.cartData[index].checked) ? ComponentConfirmationItem(
                          cartData: cartController.cartData[index],
                        ) : SizedBox(),
                      ),
                      const SizedBox(height: 150),
                  ]
                )    
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      spreadRadius: 1,
                      blurRadius: 10,
                      offset: Offset(0, -1), 
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
                  child: Wrap(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: [
                            Text("Total Harga",
                              style: GoogleFonts.dosis(
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                            Text(
                              Functions.rupiah(cartController.totalPrice),
                              style: GoogleFonts.dosis(
                                color: Colors.blue[900],
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              )
                            )
                          ]
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Ongkos Kirim",
                              style: GoogleFonts.dosis(
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                            Text(
                              Functions.rupiah(14000),
                              style: GoogleFonts.dosis(
                                color: Colors.blue[900],
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              )
                            )
                          ]
                        ),
                      ),
                      Divider(height: 10),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15.0, top: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total Tagihan",
                              style: GoogleFonts.dosis(
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                            Text(
                              Functions.rupiah(cartController.totalPrice + 14000),
                              style: GoogleFonts.dosis(
                                color: Colors.blue[900],
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              )
                            )
                          ]
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if(cartController.totalPrice != 0){
                            Functions.showLoading(context, false);
                            Future.delayed(const Duration(milliseconds: 2500), () {
                              Navigator.pop(context);
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ScreenOrderSuccess()));
                            });
                          }
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.blue[900]
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                            child: Text(
                              "BAYAR",
                              style: GoogleFonts.dosis(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              )
                            ),
                          )
                        ),
                      )
                    ]
                  ),
                )
              ),
            )
          ]
        )
      ),
    );
  }
}





