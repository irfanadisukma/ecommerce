import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:ecommerce/core/controllers/controllers.dart';
import 'package:ecommerce/core/functions/functions.dart';
import 'package:ecommerce/core/models/models.dart';
import 'package:ecommerce/ui/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

part 'screen_product_list.dart';
part 'screen_product_detail.dart';
part 'screen_cart_list.dart';
part 'screen_confirmation_order.dart';
part 'screen_order_success.dart';